import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { AppModule } from './app.module';

async function bootstrap() {
  const port = process.env.PORT || 3002;
  const logger = new Logger();
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      options: {
        transport: Transport.TCP,
        port,
        host: '0.0.0.0',
      },
    },
  );

  logger.log('starting application on TCP at ' + port);
  await app.listen();
}
bootstrap();
