import { Injectable } from '@nestjs/common';
import * as bson from 'bson';
import { PrismaService } from 'src/prisma/prisma.service';
import { InitFileDTO } from './dto/file.dto';

@Injectable()
export class FileService {
  constructor(private readonly prismaService: PrismaService) { }

  initFile(input: InitFileDTO) {
    return this.prismaService.file.create({
      data: {
        objectID: input.objectID,
        uploaderID: new bson.ObjectId().toString(),
      },
    });
  }
}
