import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/prisma/prisma.module';
import { FileService } from './file.service';

@Module({
  imports: [PrismaModule],
  providers: [FileService],
})
export class FileModule { }
